慕课网TypeScript实战

从JS到TS开发数独游戏（JS版）https://www.imooc.com/learn/899
从JS到TS开发数独游戏（TS版）https://www.imooc.com/learn/903

~/src/js
  index.js       页面入口以及事件绑定
  core/          算法及数据结构相关的脚本
    toolkit.js   工具方法集
    generator.js 生成数独解决方案
    checker.js   检查数独解决方案
    sudoku.js    生成数独游戏
  ui/            界面相关的脚本
    grid.js      生成九宫格
    popupnumbers.js 处理弹出的操作面板


每个宫计算
例如第六宫 序号n = 5(序号从0开始算)
          坐标 bX=n%3=2
               bY=n/3=1
          
          起始格坐标
               x0 = bx * 3 = 6
               y0 = by * 3 = 3

          宫内小格坐标 序号i 
               x = x0 + i%3
               y = y0 + i/3
